﻿namespace WebAPITask.Utils
{
    public static class Utils
    {
        /// <summary>
        /// Generate Unique ID
        /// </summary>
        /// <returns></returns>
        public static string GenerateUUID ()
        {
            return System.Guid.NewGuid().ToString("N");
        }
    }
}
