﻿using System.Collections.Generic;

namespace WebAPITask.Models
{
    public class User
    {
        public string Id { get; set; }
        public UserInfo Info { get; set; }
        public List<UserAppData> AppsData { get; set; }
    }
}
