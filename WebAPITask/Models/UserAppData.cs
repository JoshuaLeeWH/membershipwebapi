﻿namespace WebAPITask.Models
{
    public class UserAppData
    {
        public string Id { get; set; }
        public string AppName { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsEmailVerified { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }        
    }
}
