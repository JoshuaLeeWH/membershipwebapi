﻿using Microsoft.EntityFrameworkCore;

namespace WebAPITask.Models
{
    public class ApiContext : DbContext
    {
        public ApiContext (DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserAppDataA> AppA { get; set; }
        public DbSet<UserAppDataB> AppB { get; set; }
        public DbSet<UserAppDataC> AppC { get; set; }
    }
}
