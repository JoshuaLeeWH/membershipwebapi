﻿namespace WebAPITask.Models
{
    public class AppDataResponse
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsEmailVerified { get; set; }
    }
}
