﻿namespace WebAPITask.Models
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserLogInInfo LoginInfo { get; set; }
    }
}
