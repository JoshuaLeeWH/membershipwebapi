﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebAPITask.Models;
using Swashbuckle.AspNetCore.Swagger; 

namespace WebAPITask
{
    public class Startup
    {
        public Startup (IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services)
        {
            services.AddDbContext<ApiContext>((opt) => { opt.UseInMemoryDatabase("ApiDatabase"); });

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(
                    (options) => {
                        // set loop handling to ignore, as loading related data can cause hang without it
                        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                        // format json to have indents
                        options.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                    }
                );

            services.AddSwaggerGen((c) => 
            {
                c.SwaggerDoc("v1", new Info { Title = "WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env)
        {
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAPI V1");
                c.RoutePrefix = string.Empty; 
            }); 

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
