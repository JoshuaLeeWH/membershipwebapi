﻿using WebAPITask.Models;

namespace WebAPITask.Controllers
{
    public class UserController
    {
        /// <summary>
        /// Add a user to database
        /// </summary>
        /// <param name="num"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static User AddUser (string firstName, string lastName, ApiContext context)
        {
            var user = new User
            {
                Id = "user-" + Utils.Utils.GenerateUUID(),
                Info = new UserInfo
                {
                    FirstName = firstName,
                    LastName = lastName,
                    LoginInfo = new UserLogInInfo
                    {
                        Email = firstName + "." + lastName + "@email.com",
                        Password = "abcdefg"
                    }
                }
            };

            context.Users.Add(user);
            return user;
        }
    }
}
