﻿using WebAPITask.Models; 

namespace WebAPITask.Controllers
{
    public static class MockData
    {


        /// <summary>
        /// Add Mock Data to database
        /// </summary>
        /// <param name="context"></param>
        public static void AddMockData (ApiContext context)
        {
            User user1 = UserController.AddUser("Joshua", "Lee", context);
            User user2 = UserController.AddUser("Steph", "Curry", context);
            User user3 = UserController.AddUser("Giannis", "Antetokounmpo", context);

            AddUserToApp(user1, "A", context, isEmailVerified: true);
            AddUserToApp(user2, "a", context, isEmailVerified: true);
            AddUserToApp(user3, "A", context, isEmailVerified: true);
            AddUserToApp(user1, "b", context, isEmailVerified: true);
            AddUserToApp(user1, "C", context, isEmailVerified: true);
            AddUserToApp(user2, "B", context, isEmailVerified: true);
            AddUserToApp(user3, "c", context, isEmailVerified: true);

            context.SaveChanges();
        }


        private static void AddUserToApp (User user, string appName, ApiContext context, bool isLoggedIn = false, bool isEmailVerified = false)
        {
            switch(appName.ToLower())
            {
                case "a":
                    WebAppAController.AddUserToApp(user, appName, context, isLoggedIn, isEmailVerified); 
                    break;
                case "b":
                    WebAppBController.AddUserToApp(user, appName, context, isLoggedIn, isEmailVerified);
                    break;
                case "c":
                    WebAppCController.AddUserToApp(user, appName, context, isLoggedIn, isEmailVerified);
                    break;
                default: break; 
            }
        }
    }
}
