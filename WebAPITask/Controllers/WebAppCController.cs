﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPITask.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPITask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebAppCController : WebAppController
    {
        public WebAppCController (ApiContext context) : base(context)
        {
        }

        protected override void InitApp ()
        {
            AppName = "C";
            base.InitApp();
        }


        // GET: api/WebAppC
        [HttpGet]
        public async Task<IActionResult> Get ()
        {
            var users = await _context
                                .AppC
                                .Include(app => app.User)
                                    .ThenInclude(user => user.Info)
                                        .ThenInclude(info => info.LoginInfo)
                                .ToListAsync();

            return Ok(ConstructResponseList(users));
        }


        // GET api/WebAppC/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get (string id)
        {
            var users = await _context
                                .AppC
                                .Include(app => app.User)
                                    .ThenInclude(user => user.Info)
                                        .ThenInclude(info => info.LoginInfo)
                                .Where(app => app.UserId == id)
                                .ToListAsync();

            if(users.Count == 0)
                return NotFound();
            else if(users.Count > 1)
                return BadRequest(); 

            return Ok(ConstructResponse(users[0]));
        }


        /// <summary>
        /// Construct a list of responses
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        private IEnumerable<AppDataResponse> ConstructResponseList (IEnumerable<UserAppDataC> users)
        {
            return users.Select(u => ConstructResponse(u));
        }


        /// <summary>
        /// Construct singular response
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private AppDataResponse ConstructResponse (UserAppDataC user)
        {
            return new AppDataResponse
            {
                Id = user.Id,
                Content = user.AppSpecificContent,
                UserId = user.UserId,
                Name = user.User.Info.FirstName + " " + user.User.Info.LastName,
                Email = user.User.Info.LoginInfo.Email,
                IsLoggedIn = user.IsLoggedIn,
                IsEmailVerified = user.IsEmailVerified
            };
        }


        // POST api/WebAppC/registeruser
        [HttpPost("registeruser")]
        public async Task<ActionResult<UserAppDataC>> PostRegisterUser (UserInfo userInfo)
        {
            // don't allow user to be added if email already exists in the database
            var users = await _context
                                .Users
                                .Include(u => u.Info)
                                    .ThenInclude(info => info.LoginInfo)
                                .Where(u => u.Info.LoginInfo.Email == userInfo.LoginInfo.Email)
                                .ToListAsync();

            if(users.Count() > 0)
                return BadRequest();

            // add new user into database
            var user = new User
            {
                Id = "user-" + Utils.Utils.GenerateUUID(),
                Info = userInfo
            };
            _context.Users.Add(user);
            var userData = AddUserToApp(user, AppName, _context, isLogged: true);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = userData.Id }, userData);
        }

        
        /// <summary>
        /// AddUserToApp
        /// </summary>
        /// <param name="user"></param>
        /// <param name="appName"></param>
        /// <param name="context"></param>
        public static UserAppDataC AddUserToApp (User user, string appName, ApiContext context, bool isLogged = false, bool isEmailVerified = false)
        {
            var data = new UserAppDataC
            {
                Id = CreateAppId(user.Id, appName.ToLower()),
                AppName = appName.ToUpper(),
                UserId = user.Id,
                AppSpecificContent = CreateAppSpecificContent(user, appName.ToUpper()),
                IsLoggedIn = isLogged,
                IsEmailVerified = isEmailVerified
            };
            context.AppC.Add(data);

            return data;
        }


        // POST api/WebAppC/login
        [HttpPost("login")]
        public async Task<ActionResult<UserAppDataC>> PostLoginUser (UserLogInInfo login)
        {
            var users = await _context.AppC
                        .Include(app => app.User)
                            .ThenInclude(user => user.Info)
                                .ThenInclude(info => info.LoginInfo)
                        .Where(
                            ud =>
                            ud.User.Info.LoginInfo.Email == login.Email &&
                            ud.User.Info.LoginInfo.Password == login.Password
                        )
                        .ToListAsync();

            // email not found
            if(users.Count() == 0)
                return NotFound();
            // duplicate users with same email address found
            else if(users.Count() > 1)
                return BadRequest();
            // authentication failed
            else if(users[0].User.Info.LoginInfo.Password != login.Password)
                return Forbid();
            // successful authentiation
            else
                users[0].IsLoggedIn = true;

            return Ok(ConstructResponse(users[0]));
        }



        // POST api/WebAppC/logout
        [HttpPost("logout")]
        public async Task<ActionResult<UserAppDataC>> PostLogoutUser (UserLogInInfo login)
        {
            var users = await _context.AppC
                        .Include(app => app.User)
                            .ThenInclude(user => user.Info)
                                .ThenInclude(info => info.LoginInfo)
                        .Where(
                            ud =>
                            ud.User.Info.LoginInfo.Email == login.Email
                        )
                        .ToListAsync();

            // email not found
            if(users.Count() == 0)
                return NotFound();
            // duplicate users with same email address found
            else if(users.Count() > 1)
                return BadRequest();
            // successful log out
            else
                users[0].IsLoggedIn = false;

            return Ok(ConstructResponse(users[0]));
        }


        // PUT api/WebAppC/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put (string id, UserAppDataC user)
        {
            if(id != user.Id)
                return BadRequest();

            return await PutUser(user);
        }


        /// <summary>
        /// PutUser data
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<IActionResult> PutUser (UserAppDataC user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }


        // DELETE api/WebAppC/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete (string id)
        {
            var user = await _context.AppC.FindAsync(id);
            return await DeleteUser(user);
        }


        /// <summary>
        /// DeleteUser data
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<IActionResult> DeleteUser (UserAppDataC user)
        {
            if(user == null)
                return NotFound();

            _context.AppC.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
