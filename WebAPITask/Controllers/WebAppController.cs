﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPITask.Models;

namespace WebAPITask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebAppController : ControllerBase
    {
        protected readonly ApiContext _context;
        protected string AppName; 

        public WebAppController (ApiContext context)
        {
            _context = context;
            InitApp(); 
        }


        /// <summary>
        /// Add Initialisation setup here
        /// </summary>
        protected virtual void InitApp ()
        {
            // create mock data if empty
            if(_context.Users.Count() == 0)
                MockData.AddMockData(_context);
        }
                       

        protected static string CreateAppId (string userId, string app)
        {
            return "app" + app + "_" + userId;
        }

        protected static string CreateAppSpecificContent (User user, string app)
        {
            return user.Info.FirstName + " " + user.Info.LastName + " in app" + app;
        }
    }
}
