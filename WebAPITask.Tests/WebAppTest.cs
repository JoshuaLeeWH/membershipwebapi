using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPITask.Controllers;
using WebAPITask.Models;

namespace WebAPITask.Tests
{
    public class WebAppTest
    {
        ApiContext InitDbContext ()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
            optionsBuilder.UseInMemoryDatabase("ApiDatabase");
            var context = new ApiContext(optionsBuilder.Options);
            return context; 
        }


        [Fact]
        public async Task Get_ResultsFound ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);

            // act
            var result = await appA.Get();

            // assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetId_NotFound ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);

            // act
            var result = await appA.Get(Utils.Utils.GenerateUUID());

            // assert
            Assert.IsNotType<OkObjectResult>(result);
        }

        [Fact]
        public async Task PostRegisterUser_EmailAddressAlreadyExists ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);
            UserInfo userInfo = new UserInfo
            {
                FirstName =  "Joshua",
                LastName =  "Lee",
                LoginInfo = new UserLogInInfo
                {
                    Email = "Joshua.Lee@email.com",
                    Password = "askdljldksj"
                }
            };

            // act
            var result = await appA.PostRegisterUser(userInfo);

            // assert
            var actionResult = Assert.IsType<ActionResult<UserAppDataA>>(result);
            Assert.IsNotType<CreatedAtActionResult>(actionResult.Result);
        }


        [Fact]
        public async Task PostRegisterUser_Successful ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);
            UserInfo userInfo = new UserInfo
            {
                FirstName =  "SignMe",
                LastName =  "UPPP",
                LoginInfo = new UserLogInInfo
                {
                    Email = "SignMe.UPPP@email.com",
                    Password = "askdljldksj"
                }
            };

            // act
            var result = await appA.PostRegisterUser(userInfo);

            // assert
            var actionResult = Assert.IsType<ActionResult<UserAppDataA>>(result);
            Assert.IsType<CreatedAtActionResult>(actionResult.Result);
        }


        [Fact]
        public async Task PostLogin_AuthenticationFailed ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);
            UserLogInInfo login = new UserLogInInfo
            {
                Email = "Joshua.Lee@email.com",
                Password = "wrongpassword"
            };

            // act
            var result = await appA.PostLoginUser(login);

            // assert
            var actionResult = Assert.IsType<ActionResult<UserAppDataA>>(result);
            Assert.IsNotType<OkObjectResult>(result.Result);
        }

        [Fact]
        public async Task PostLogin_Success ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);
            UserLogInInfo login = new UserLogInInfo
            {
                Email = "Joshua.Lee@email.com",
                Password = "abcdefg"
            };

            // act
            var result = await appA.PostLoginUser(login);

            // assert
            var actionResult = Assert.IsType<ActionResult<UserAppDataA>>(result);
            Assert.IsType<OkObjectResult>(result.Result);
        }


        [Fact]
        public async Task PostLogout_NotFound ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);
            UserLogInInfo login = new UserLogInInfo
            {
                Email = "weeeeee.pooo@email.com"
            };

            // act
            var result = await appA.PostLogoutUser(login);

            // assert
            var actionResult = Assert.IsType<ActionResult<UserAppDataA>>(result);
            Assert.IsNotType<OkObjectResult>(result.Result);
        }


        [Fact]
        public async Task PostLogout_Success ()
        {
            // arrange
            var db = InitDbContext();
            WebAppAController appA = new WebAppAController(db);
            UserLogInInfo login = new UserLogInInfo
            {
                Email = "Joshua.Lee@email.com"
            };

            // act
            var result = await appA.PostLogoutUser(login);

            // assert
            var actionResult = Assert.IsType<ActionResult<UserAppDataA>>(result);
            Assert.IsType<OkObjectResult>(result.Result);
        }
    }
}
