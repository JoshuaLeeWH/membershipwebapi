# MembershipWebAPI by Joshua Lee

Membership Web API using ASP.NET CORE.

Technologies used: 
+ ASP.NET CORE 2.2.0,
+ Postman,
+ Swashbuckle,
+ Swagger
+ XUnit
+ Docker

Docker image url: 
joshualeewh/membership-web-api


Run Instructions - using Docker
--------------------------------
+ Grab the docker image by running:
  'docker pull joshualeewh/membership-web-api:latest'
+ Run the image: 
  'docker run -itd -p 8080:80 joshualeewebapi --entrypoint'
+ Via browser, go to 'localhost:8080', 
  you should now see SwaggerUI displaying the WebAPI V1.
+ Alternatively, you can use Postman to do GET, POSTs calls.
  

  
Run Instructions - via Source Code and Visual Studio
-----------------------------------------------------
+ Clone the repo to your folder of choice
  'git clone https://JoshuaLeeWH@bitbucket.org/JoshuaLeeWH/membershipwebapi.git'
+ Open project in Visual Studio, and 'Debug/Start Debugging' or press F5 to launch the app in a browser.
+ You should now see SwaggerUI displaying the WebAPI V1.
+ Alternatively, you can use Postman to do GET, POSTs calls.



Functionality
----------------
+ Centralised membership service for three WebApps, A, B and C.
+ WebAPI is only accessible via the WebApps.
+ Via WebAppA, WebAppB, or WebAppC, create, login, and logout users.
+ Login state is independent of the App, i.e. userA can be logged in to WebAppA, but not to WebAppB.
+ Mock data is created on startup.
+ Get all users signed up to the App. 
+ SwaggerUI integration using Swashbuckle.
+ XUnit unit testing integrated.
+ Unit tests, testing Result Found/NotFound, Register User Fail/Succeed, Login Fail/Succeed, and Logout Fail/Succeed.



Register User data needed: 
{
   FirstName, 
   LastName, 
   LoginInfo: {
      Email, 
	  Password
   }   
}

Login Data needed: 
{
   Email, 
   Password
}


Logout Data needed: 
{
   Password
}